Source: genomicsdb
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Pierre Gruet <pgt@debian.org>
Build-Depends: debhelper-compat (= 13),
               cmake,
               default-jdk,
               javahelper,
               libbz2-dev,
               libcatch2-dev,
               libcurlpp-dev,
               libfmt-dev,
               libmuparserx-dev,
               libprotobuf-dev,
               libspdlog-dev,
               libssl-dev,
               liblzma-dev,
               maven-debian-helper,
               mpi-default-dev,
               protobuf-compiler,
               rapidjson-dev,
               testng <!nocheck>,
               uuid-dev
Build-Depends-Arch: chrpath
Build-Depends-Indep: libbuild-helper-maven-plugin-java,
                     libcommons-io-java,
                     libcommons-lang3-java,
                     libgetopt-java,
                     libguava-java,
                     libhtsjdk-java,
                     libjson-simple-java,
                     liblog4j2-java,
                     libmaven-antrun-plugin-java,
                     libprotobuf-java,
                     libsurefire-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/genomicsdb
Vcs-Git: https://salsa.debian.org/med-team/genomicsdb.git
Homepage: https://www.genomicsdb.org/
Rules-Requires-Root: no

Package: libgenomicsdb-java
Section: java
Architecture: all
Depends: ${maven:Depends},
         ${java:Depends},
         libgenomicsdb-jni (>= ${source:Version}),
         libgenomicsdb-jni (<< ${source:Version}.1~),
         ${misc:Depends}
Description: sparse array storage library for genomics (Java library)
 GenomicsDB is built on top of a htslib fork and an internal array storage
 system for importing, querying and transforming variant data. Variant data is
 sparse by nature (sparse relative to the whole genome) and using sparse array
 data stores is a perfect fit for storing such data.
 .
 The GenomicsDB stores variant data in a 2D array where:
  - Each column corresponds to a genomic position (chromosome + position);
  - Each row corresponds to a sample in a VCF (or CallSet in the GA4GH
    terminology);
  - Each cell contains data for a given sample/CallSet at a given position;
    data is stored in the form of cell attributes;
  - Cells are stored in column major order - this makes accessing cells with
    the same column index (i.e. data for a given genomic position over all
    samples) fast.
  - Variant interval/gVCF interval data is stored in a cell at the start of the
    interval. The END is stored as a cell attribute. For variant intervals
    (such as deletions and gVCF REF blocks), an additional cell is stored at
    the END value of the variant interval. When queried for a given genomic
    position, the query library performs an efficient sweep to determine all
    intervals that intersect with the queried position.
 .
 This package contains the Java library.

Package: libgenomicsdb-jni
Section: java
Architecture: any
Depends: ${shlibs:Depends},
         libgenomicsdb1 (= ${binary:Version}),
         ${misc:Depends}
Description: sparse array storage library for genomics (Java native bindings)
 GenomicsDB is built on top of a htslib fork and an internal array storage
 system for importing, querying and transforming variant data. Variant data is
 sparse by nature (sparse relative to the whole genome) and using sparse array
 data stores is a perfect fit for storing such data.
 .
 The GenomicsDB stores variant data in a 2D array where:
  - Each column corresponds to a genomic position (chromosome + position);
  - Each row corresponds to a sample in a VCF (or CallSet in the GA4GH
    terminology);
  - Each cell contains data for a given sample/CallSet at a given position;
    data is stored in the form of cell attributes;
  - Cells are stored in column major order - this makes accessing cells with
    the same column index (i.e. data for a given genomic position over all
    samples) fast.
  - Variant interval/gVCF interval data is stored in a cell at the start of the
    interval. The END is stored as a cell attribute. For variant intervals
    (such as deletions and gVCF REF blocks), an additional cell is stored at
    the END value of the variant interval. When queried for a given genomic
    position, the query library performs an efficient sweep to determine all
    intervals that intersect with the queried position.
 .
 This package contains the Java native bindings.

Package: genomicsdb-tools
Architecture: any
Depends: ${shlibs:Depends},
         libgenomicsdb1 (= ${binary:Version}),
         ${misc:Depends}
Description: sparse array storage library for genomics (tools)
 GenomicsDB is built on top of a htslib fork and an internal array storage
 system for importing, querying and transforming variant data. Variant data is
 sparse by nature (sparse relative to the whole genome) and using sparse array
 data stores is a perfect fit for storing such data.
 .
 The GenomicsDB stores variant data in a 2D array where:
  - Each column corresponds to a genomic position (chromosome + position);
  - Each row corresponds to a sample in a VCF (or CallSet in the GA4GH
    terminology);
  - Each cell contains data for a given sample/CallSet at a given position;
    data is stored in the form of cell attributes;
  - Cells are stored in column major order - this makes accessing cells with
    the same column index (i.e. data for a given genomic position over all
    samples) fast.
  - Variant interval/gVCF interval data is stored in a cell at the start of the
    interval. The END is stored as a cell attribute. For variant intervals
    (such as deletions and gVCF REF blocks), an additional cell is stored at
    the END value of the variant interval. When queried for a given genomic
    position, the query library performs an efficient sweep to determine all
    intervals that intersect with the queried position.
 .
 This package contains some tools to be run as executable files.

Package: libgenomicsdb1
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         mpi-default-bin
Breaks: libgenomicsdb0 (<< 1.5.3)
Replaces: libgenomicsdb0 (<< 1.5.3)
Description: sparse array storage library for genomics (shared libraries)
 GenomicsDB is built on top of a htslib fork and an internal array storage
 system for importing, querying and transforming variant data. Variant data is
 sparse by nature (sparse relative to the whole genome) and using sparse array
 data stores is a perfect fit for storing such data.
 .
 The GenomicsDB stores variant data in a 2D array where:
  - Each column corresponds to a genomic position (chromosome + position);
  - Each row corresponds to a sample in a VCF (or CallSet in the GA4GH
    terminology);
  - Each cell contains data for a given sample/CallSet at a given position;
    data is stored in the form of cell attributes;
  - Cells are stored in column major order - this makes accessing cells with
    the same column index (i.e. data for a given genomic position over all
    samples) fast.
  - Variant interval/gVCF interval data is stored in a cell at the start of the
    interval. The END is stored as a cell attribute. For variant intervals
    (such as deletions and gVCF REF blocks), an additional cell is stored at
    the END value of the variant interval. When queried for a given genomic
    position, the query library performs an efficient sweep to determine all
    intervals that intersect with the queried position.
 .
 This package contains the shared library.

Package: libgenomicsdb-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         libgenomicsdb1 (= ${binary:Version}),
         ${misc:Depends}
Description: sparse array storage library for genomics (development files)
 GenomicsDB is built on top of a htslib fork and an internal array storage
 system for importing, querying and transforming variant data. Variant data is
 sparse by nature (sparse relative to the whole genome) and using sparse array
 data stores is a perfect fit for storing such data.
 .
 The GenomicsDB stores variant data in a 2D array where:
  - Each column corresponds to a genomic position (chromosome + position);
  - Each row corresponds to a sample in a VCF (or CallSet in the GA4GH
    terminology);
  - Each cell contains data for a given sample/CallSet at a given position;
    data is stored in the form of cell attributes;
  - Cells are stored in column major order - this makes accessing cells with
    the same column index (i.e. data for a given genomic position over all
    samples) fast.
  - Variant interval/gVCF interval data is stored in a cell at the start of the
    interval. The END is stored as a cell attribute. For variant intervals
    (such as deletions and gVCF REF blocks), an additional cell is stored at
    the END value of the variant interval. When queried for a given genomic
    position, the query library performs an efficient sweep to determine all
    intervals that intersect with the queried position.
 .
 This package contains the development files and the static library.
