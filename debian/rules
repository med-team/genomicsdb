#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
include /usr/share/dpkg/buildflags.mk
CFLAGS+=$(CPPFLAGS)
CXXFLAGS+=$(CPPFLAGS)
DPKG_EXPORT_BUILDFLAGS = 1

include /usr/share/dpkg/pkg-info.mk

export JAVA_HOME = /usr/lib/jvm/default-java

%:
	dh $@ --buildsystem=cmake --with javahelper

override_dh_auto_clean:
	# Removing the files we provided manually for the build of the component
	if [ -e genomicsdb-htslib/CMakeLists.txt ]; then rm genomicsdb-htslib/CMakeLists.txt; fi
	if [ -e genomicsdb-htslib/config.h ]; then rm genomicsdb-htslib/config.h; fi
	if [ -e genomicsdb-htslib/version.h ]; then rm genomicsdb-htslib/version.h; fi
	dh_auto_clean
	dh_auto_clean --buildsystem=maven
	# Removing the debian/maven.properties file we setup during configure step.
	if [ -e debian/maven.properties ]; then rm debian/maven.properties; fi
	# Removing the patched pom.xml file.
	if [ -e pom.xml.old ]; then mv pom.xml.old pom.xml; fi

override_dh_auto_configure-arch:
	# Preparing the build of the htslib component.
	cp debian/htslibComponentFiles/* genomicsdb-htslib/
	echo '#define HTS_VERSION_TEXT "$(HTS_COMPONENT_VERSION)"' > genomicsdb-htslib/version.h
	# Configuring the C++ part using CMake.
	dh_auto_configure -- \
	        -DBUILD_JAVA:BOOL=False \
	        -DCMAKE_INSTALL_PREFIX:PATH=/usr \
	        -DJAVA_HOME:PATH=$(JAVA_HOME) \
	        -DPROTOBUF_ROOT_DIR:PATH=/usr \
	        -DCMAKE_LIBRARY_ARCHITECTURE:STRING=$(DEB_HOST_MULTIARCH)

override_dh_auto_configure-indep: override_dh_auto_configure-arch
	# Configuring the Java part using Maven.
	# First we put the arch triplet in d/maven.properties for the tests.
	echo "build.triplet=$(DEB_HOST_MULTIARCH)" > debian/maven.properties
	mv pom.xml pom.xml.old
	sed 's/\$${genomicsdb\.version}/$(DEB_VERSION_UPSTREAM)/; s,\$${protoc\.filepath},/usr/bin/protoc,' pom.xml.old > pom.xml
	dh_auto_configure --buildsystem=maven

# Trick to be able to build the arch-dependent binaries before the indep- tests.
override_dh_auto_build-arch:
	dh_auto_build

override_dh_auto_build-indep:
	# Building the Java part with Maven.
	dh_auto_build --buildsystem=maven

# Ignoring test failures so that dependencies of arch-dep on arch-indep can be
# satisfied for this package. We have autopkgtests. 
override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test -a || true
endif

# We need the arch-dependent binaries to run the Java tests afterwards.
override_dh_auto_test-indep: override_dh_auto_build-arch
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# Testing the Java part. We extend LD_LIBRARY_PATH because some test code is
	# run without surefire.
	LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:obj-$(DEB_HOST_MULTIARCH)/src/main" dh_auto_test --buildsystem=maven
endif

override_dh_auto_install-indep:
	# Installing the Java part.
	dh_auto_install --buildsystem=maven

execute_after_dh_install-arch:
	# Setting the runpath of the jni library to only contain the path to the private libs.
	chrpath -r "/usr/lib/$(DEB_HOST_MULTIARCH)/genomicsdb" $(CURDIR)/debian/libgenomicsdb-jni/usr/lib/jni/libgenomicsdbjni.so
